import app.data.dummydata as dummydata
import unittest
from app.services import book_service
import app.models.Book as modelbook


""" Tests the remove_book() function to make sure it is working as expected """
class TestRemoveBook(unittest.TestCase):

    """ Runs before every test case so they have a list of books """
    def setUp(self):
        self.method = book_service.BookService()
        self.booklist = self.method.get_all_books()
        self.testbook = modelbook.Book
        
    
    """  Runs after every test case, removes the list from setUp """
    def tearDown(self):
        self.booklist = None
        self.book = None
    
    """
        Test to see if the function is given a book_id 1 it will return the
        right book
    """
    def test_delete_book_with_id_1(self):
        fake_id = 1
        book_count = len(self.booklist)
        self.method.remove_book(fake_id)
        self.assertEqual(book_count -1, len(self.booklist))

    """ Test removing a book that does not exist in the system. """
    def test_id_not_exists(self):
        fake_id = 1337
        book_count = len(self.booklist)
        self.method.remove_book(fake_id) #should not remove any book
        self.assertEqual(book_count, len(self.booklist))

    """ Test removing a book with a wrong type of parameter value. """
    def test_invalid_parameter(self):
        fake_id = "b"
        book_count = len(self.booklist)
        self.method.remove_book(fake_id) #should not remove any book
        self.assertEqual(book_count, len(self.booklist))

if __name__ == "__main__":
    unittest.main()
