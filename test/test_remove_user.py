import app.data.dummydata as dummydata
from app.services import user_service
from app.models import User
import unittest

""" Tests the functionality of removing a certain user from the system """
class TestRemoveUser(unittest.TestCase):
    """ Code that is run before every test case """
    def setUp(self):
        self.__user_service = user_service.UserService()

    """ Code that is run after every test case """
    def tearDown(self):
        dummydata.users = {1: dummydata.user1, 2: dummydata.user2, 3: dummydata.user3}
    
    """ Tests if the remove_user() method doesn't allow the removal of admins """
    def test_user_isadmin(self):
        fakeid = 1
        self.assertEqual(self.__user_service.remove_user(fakeid), "Cannot remove an admin from the system!")

    """ Tests if the remove_user() method actually removes the specified user """
    def test_user_exists(self):
        fakeid = 2
        self.__user_service.remove_user(fakeid)
        self.assertEqual(self.__user_service.get_all_users(), {1: dummydata.user1, 3: dummydata.user3})

    """
        Tests if the remove_user() method returns an error message if the
        specified ID does not exist.
    """
    def test_user_not_exists(self):
        fakeid = 5
        self.assertEqual(self.__user_service.remove_user(fakeid), "User does not exist!")

    """
        Tests if the remove_user() method returns an error message if the ID is
        not of the right type
    """
    def test_unexpected_input(self):
        fakeid = "d"
        self.assertEqual(self.__user_service.remove_user(fakeid), "Id must be int!")
    

if __name__ == "__main__":
       unittest.main()