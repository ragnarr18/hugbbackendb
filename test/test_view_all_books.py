from app.data.dummydata import *
import app.services.book_service as book_service
import unittest

""" A class that tests the retrieval of a list of all books """
class TestGetBookList(unittest.TestCase):
    """ Code that is run before every test. """
    def setUp(self):
        self.__book_service = book_service.BookService()

    """ Tests if the get_all_books() method returns all book objects """
    def test_view_all_books(self):
        Books = {1: book1, 2: book2, 3: book3}
        self.assertEqual(self.__book_service.get_all_books(), Books)

    """
        Tests if the get_all_books() method doesn't return just an empty string
    """
    def test_compare_wrong_results(self):
        self.assertNotEqual(self.__book_service.get_all_books(),"")
    
    """
        Test if the length of the results of get_all_books() and the books
        stored in dummydata is the same.
    """
    def test_compare_length_of_results(self):
        result = self.__book_service.get_all_books()
        Blen = len({1: book1, 2: book2, 3: book3})
        self.assertEqual(len(result), Blen)


if __name__ == "__main__":
    unittest.main()