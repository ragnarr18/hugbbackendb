import PySimpleGUI as sg
# All the stuff inside your window.
def little_test():
    layout = [  [sg.Text('Some text on Row 1')],
                [sg.Text('Enter something on Row 2'), sg.InputText()],
                [sg.Button('Ok'), sg.Button('Cancel')] ]

    # Create the Window
    window = sg.Window('Window Title', layout)
    # Event Loop to process "events" and get the "values" of the inputs
    while True:             
        event, values = window.read()
        if event in (None, 'Cancel'):   # if user closes window or clicks cancel
            break

    window.close()

def read_file():

    layout = [[sg.Text('Filename')],
            [sg.Input(), sg.FileBrowse()], 
        [sg.OK(), sg.Cancel()]] 

    window = sg.Window('Get filename example', layout)

    event, values = window.Read()

read_file()
