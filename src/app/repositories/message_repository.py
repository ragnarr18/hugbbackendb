import json

"""
The repository class for messages. Takes care of the database for messages and has methods to return
data and change it.
"""
class MessageRepository(object):
    """ Returns all the messages in the database """
    def get_all_messages(self):
        all_messages = {}
        with open("app/data/messagedata.json", "r") as filestream:
            all_messages = json.loads(filestream.read())
        return all_messages
    
    """ Finds a message with the specified message ID and returns it """
    def get_message(self, message_id):
        all_messages = self.get_all_messages()
        return all_messages[str(message_id)]

    """ Adds a new message to the database """
    def send_message(self, message_obj):
        all_messages = {}
        with open("app/data/messagedata.json", "r") as filestream:
            all_messages = json.loads(filestream.read())
        all_messages[str(message_obj["id"])] = message_obj
        with open("app/data/messagedata.json", "w") as filestream:
            filestream.write(json.dumps(all_messages))
        self.increment_next_message_id()
        return message_obj
    
    """ Returns the next available ID for a new message """
    def get_next_message_id(self):
        all_ids = {}
        with open("app/data/ids.json", "r") as filestream:
            all_ids = json.loads(filestream.read())
        return all_ids["next_message_id"]
    
    """ Returns True if a message exists, otherwise False """
    def message_exists(self, message_id):
        all_messages = self.get_all_messages()
        return str(message_id) in all_messages
    
    """ Permanently deletes a message from messagedata.json """
    def hard_delete_message(self, message_id):
        all_messages = self.get_all_messages()
        del all_messages[str(message_id)]
        with open("app/data/messagedata.json", "w") as filestream:
            filestream.write(json.dumps(all_messages))
    
    """ Decrements the next available message ID in ids.json """
    def decrement_next_message_id(self):
        all_ids = {}
        with open("app/data/ids.json", "r") as filestream:
            all_ids = json.loads(filestream.read())
        all_ids["next_message_id"] -= 1
        with open("app/data/ids.json", "w") as filestream:
            filestream.write(json.dumps(all_ids))
    
    """ Increments the next available message ID """
    def increment_next_message_id(self):
        all_ids = {}
        with open("app/data/ids.json", "r") as filestream:
            all_ids = json.loads(filestream.read())
        all_ids["next_message_id"] += 1
        with open("app/data/ids.json", "w") as filestream:
            filestream.write(json.dumps(all_ids))