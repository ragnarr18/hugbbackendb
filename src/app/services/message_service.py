from app.repositories.message_repository import MessageRepository
from app.repositories.user_repository import UserRepository

"""
The service class for messages. Checks the input the user gives and calls the Repository layer to
recieve data or change it.
"""
class MessageService(object):
    """ Initializes the class with an instance of the class MessageRepository """
    def __init__(self):
        self.__message_repository = MessageRepository()
        self.__user_repository = UserRepository()
    
    """ Adds a message to the database """
    def send_message(self, message_dict):
        self.__validate_message_dict(message_dict)
        message_dict["id"] = self.__message_repository.get_next_message_id()
        return self.__message_repository.send_message(message_dict)
    
    """ Returns all the messages registered in the system. """
    def get_all_messages(self):
        return self.__message_repository.get_all_messages()

    """ Returns all messages that are to the user that has the same id as the user_id input """
    def get_messages_to(self, user_id):
        if not self.__user_repository.user_exists(user_id):
            raise ValueError(f"Cannot get messages to user with ID={user_id} since user does not exist")
        all_messages = self.__message_repository.get_all_messages()
        messages_to = {}
        for message in all_messages.values():
            if str(message["receiver_id"]) == str(user_id):
                messages_to[str(message["id"])] = message
        return messages_to

    """ Returns all messages that have been sent by the user that has the same id as the user_id input """
    def get_messages_by(self, user_id):
        if not self.__user_repository.user_exists(user_id):
            raise ValueError(f"Cannot get messages by user with ID={user_id} since user does not exist")
        all_messages = self.__message_repository.get_all_messages()
        messages_by = {}
        for message in all_messages.values():
            if str(message["sender_id"]) == str(user_id):
                messages_by[str(message["id"])] = message
        return messages_by

    """  Returns all messages that were sent by the user with id userid1 and were sent to the user with id userid2  """
    def get_messages_between(self, userid1, userid2):
        if not self.__user_repository.user_exists(userid1):
            raise ValueError(f"Cannot get messages by user with ID={userid1} since user does not exist")
        if not self.__user_repository.user_exists(userid2):
            raise ValueError(f"Cannot get messages by user with ID={userid2} since user does not exist")
        all_messages = self.__message_repository.get_all_messages()
        messages_between = {}
        check_list = [str(userid1), str(userid2)]
        for message in all_messages.values():
            if str(message["sender_id"]) in check_list and str(message["receiver_id"] in check_list):
                messages_between[str(message["id"])] = message
        return messages_between
    
    """ Returns information about a message with the specified message id """
    def get_message(self, message_id):
        if not self.__message_repository.message_exists(message_id):
            raise ValueError(f"Cannot get info about message with ID={message_id} since it does not exist")
        return self.__message_repository.get_message(message_id)
    
    """ Permanently deletes a message from the system (only use this in tests!) """
    def hard_delete_message(self, message_id):
        if not self.__message_repository.message_exists(message_id):
            raise ValueError(f"Cannot get info about message with ID={message_id} since it does not exist")
        self.__message_repository.hard_delete_message(message_id)
    
    """ Decrements the next available ID for new messages (only use this in tests!) """
    def decrement_next_message_id(self):
        self.__message_repository.decrement_next_message_id()
        
    """ Checks if a message dictionary is valid """
    def __validate_message_dict(self, message_dict):
        if message_dict == None:
            raise ValueError("Some required information was not given")
        has_valid_properties, message = self.__check_message_dict_properties(message_dict)
        if not has_valid_properties:
            raise ValueError(message)
        has_valid_types, message = self.__check_message_dict_value_types(message_dict)
        if not has_valid_types:
            raise ValueError(message)
    
    """ Checks if a book dictionary has only the valid properties """
    def __check_message_dict_properties(self, message_dict):
        properties = ["message", "sender_id", "receiver_id", "timestamp"]
        for prop in properties:
            if prop not in message_dict:
                return (False, "Model is missing property: {}".format(prop))
        if len(message_dict.keys()) != len(properties):
            return (False, "Model includes some unwanted properties")
        return (True, "All is well")
    
    """ Checks if a book dictionary has all the valid value types """
    def __check_message_dict_value_types(self, message_dict):
        if  type(message_dict["message"]) != str or \
            type(message_dict["sender_id"]) != int or \
            type(message_dict["receiver_id"]) != int or \
            type(message_dict["timestamp"]) != str:
            return (False, "Some property/properties are not of the correct type")
        return (True, "All is well")