import unittest
import asyncio
import json
from app.client import client

class TestSystemRental(unittest.TestCase):
    def setUp(self):
        dummy_rental = {
            "lender_id": -1,
            "renter_id": -2,
            "book_id": -1
        }
        body = {
            "op": "create_rental",
            "rental_model": dummy_rental
        }
        result = asyncio.get_event_loop() \
                        .run_until_complete(client.send_test_message(body))
        self.__dummy_rental = json.loads(result)
    
    """ Method that is run after every test case """
    def tearDown(self):
        body = { "op": "hard_delete_rental", "rental_id": int(self.__dummy_rental["id"]) }
        asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        body = { "op": "decrement_next_rental_id" }
        asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
    
    """ Tests if the create_rental method returns a correct rental model. """
    def test_create_rental_returns_rental_model(self):
        self.assertEqual(type(self.__dummy_rental), dict)
        self.assertTrue("id" in self.__dummy_rental)
        self.assertTrue("lender_id" in self.__dummy_rental)
        self.assertTrue("renter_id" in self.__dummy_rental)
        self.assertTrue("book_id" in self.__dummy_rental)
    
    """ Tests if the get_rental_info method returns correct rental. """
    def test_get_rental_info_returns_correct_rental(self):
        # Get info from the rental
        body = {
            "op": "get_rental_info",
            "rental_id": self.__dummy_rental["id"]
        }
        return_val = asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        rental_info = json.loads(return_val)

        # Check if info matches
        self.assertEqual(rental_info["id"], self.__dummy_rental["id"])
        self.assertEqual(rental_info["lender_id"], self.__dummy_rental["lender_id"])
        self.assertEqual(rental_info["renter_id"], self.__dummy_rental["renter_id"])
        self.assertEqual(rental_info["book_id"], self.__dummy_rental["book_id"])
    
    """ Tests if close_rental marks the rental correctly. """
    def test_close_rental_marks_rental_as_closed(self):
        # Try removing dummy book
        body = {
            "op": "close_rental",
            "rental_id": self.__dummy_rental["id"],
            "paid": True,
            "lender_review_model": {}
        }
        asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        
        # See if its status == 2
        body = {
            "op": "get_rental_info",
            "rental_id": self.__dummy_rental["id"]
        }
        return_val = asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        rental_model = json.loads(return_val)
        self.assertEqual(rental_model["status"], 2)

if __name__ == "__main__":
    unittest.main()