import unittest
from app.services.book_service import BookService
from app.repositories.book_repository import BookRepository

""" Tests the edit_book() function """
class testEditBook(unittest.TestCase):
    """ Sets up the test for each test method"""
    def setUp(self):
        self.__bookservice = BookService()
        self.__bookrepository = BookRepository()
        self.booklist = self.__bookservice.get_all_books()
        self.delete = False
        self.testbook = {
        "user_id": 2,
        "name": "Harry Potter and the Chamber of Secrets",
        "authors": ["J.K. Rowling"],
        "genres": ["Fantasy"],
        "edition": 1,
        "published": 1999,
        "condition": 1,
        }

    """ Cleans up for the next test method """
    def tearDown(self):
        self.booklist = None
        self.testbook = None
        if self.delete:
            self.__bookservice.hard_delete_book(self.__bookrepository.get_next_book_id()-1)
            self.__bookservice.decrement_next_book_id()

    """ Tests editing a book and inputting no name """
    def test_edit_book_missing_input(self):
        self.new_values = {
        "user_id": 2,
        "name": "",
        "authors": ["J.K. Rowling"],
        "genres": ["Fantasy"],
        "edition": 1,
        "published": 1999,
        "condition": 1,
        }

        with self.assertRaises(ValueError):
            self.__bookservice.edit_book(self.testbook, self.new_values)

    """ Tests editing a book and missing a parameter """
    def test_edit_book__input(self):
        self.new_values = {
        "user_id": 2,
        "name": "Harry Potter and the Chamber of Secrets",
        "authors": ["J.K. Rowling"],
        "genres": ["Fantasy"],
        "edition": 1,
        "published": None,
        "condition": 1,
        }
        with self.assertRaises(ValueError):
            self.__bookservice.edit_book(self.testbook, self.new_values)

    """ Tests editing a book, but making no changes """
    def test_edit_book_no_changes(self):
        self.new_values = {
        "user_id": 2,
        "name": "Harry Potter and the Chamber of Secrets",
        "authors": ["J.K. Rowling"],
        "genres": ["Fantasy"],
        "published": 1999,
        "edition": 1,
        "condition": 1,
        }
        self.delete = True
        self.assertEqual(self.__bookservice.edit_book(self.testbook, self.new_values), None)



if __name__ == "__main__":
    unittest.main()