import unittest
import asyncio
import json
from app.client import client

"""
    Tests the exchange-related functionality of the server/system
"""
class TestSystemBook(unittest.TestCase):
    def setUp(self):
        dummy_exchange = {
            "userid1": 1,
            "userid2": 2,
            "bookid1": -2,
            "bookid2": -1
        }
        body = {
            "op": "create_exchange",
            "exchange_model": dummy_exchange
        }
        result = asyncio.get_event_loop() \
                        .run_until_complete(client.send_test_message(body))
        self.__dummy_exchange = json.loads(result)
    
    """ Method that is run after every test case """
    def tearDown(self):
        body = { "op": "hard_delete_exchange", "exchange_id": int(self.__dummy_exchange["id"]) }
        asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        body = { "op": "decrement_next_exchange_id" }
        asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
    
    """ Tests if the add_exchange method returns the added exchange. """
    def test_add_exchange_returns_exchange_model(self):
        self.assertEqual(type(self.__dummy_exchange), dict)
        self.assertTrue("id" in self.__dummy_exchange)
        self.assertTrue("userid1" in self.__dummy_exchange)
        self.assertTrue("userid2" in self.__dummy_exchange)
        self.assertTrue("bookid1" in self.__dummy_exchange)
        self.assertTrue("bookid2" in self.__dummy_exchange)
        self.assertTrue("status" in self.__dummy_exchange)
    
    """ Tests if the remove_exchange method marks the exchange correctly. """
    def test_remove_exchange_marks_exchange_as_removed(self):
        # Try removing dummy exchange
        body = {
            "op": "remove_exchange",
            "exchange_id": self.__dummy_exchange["id"]
        }
        asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        
        # See if its status == 3
        body = {
            "op": "get_exchange_info",
            "exchange_id": self.__dummy_exchange["id"]
        }
        return_val = asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        exchange_model = json.loads(return_val)
        self.assertEqual(exchange_model["status"], 3)
    
    """ Tests if the get_exchange_info method returns correct exchange. """
    def test_get_exchange_info_returns_correct_exchange(self):
        # Get info from the exchange
        body = {
            "op": "get_exchange_info",
            "exchange_id": self.__dummy_exchange["id"]
        }
        return_val = asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        exchange_info = json.loads(return_val)

        # Check if info matches
        self.assertEqual(exchange_info["id"], self.__dummy_exchange["id"])
        self.assertEqual(exchange_info["userid1"], self.__dummy_exchange["userid1"])
        self.assertEqual(exchange_info["userid2"], self.__dummy_exchange["userid2"])
        self.assertEqual(exchange_info["bookid1"], self.__dummy_exchange["bookid1"])
        self.assertEqual(exchange_info["bookid2"], self.__dummy_exchange["bookid2"])
        self.assertEqual(exchange_info["status"], self.__dummy_exchange["status"])
    
    """ Tests if get_all_exchanges method returns all exchanges. """
    def test_get_all_exchanges_returns_non_empty_dict(self):
        body = { "op": "get_all_exchanges" }
        return_val = asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        all_exchanges = json.loads(return_val)
        self.assertTrue(type(all_exchanges) == dict)
        self.assertTrue(len(all_exchanges) > 0)

if __name__ == "__main__":
    unittest.main()