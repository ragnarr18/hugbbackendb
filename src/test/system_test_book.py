import unittest
import asyncio
import json
from app.client import client

"""
    Tests the book-related functionality of the server/system
"""
class TestSystemBook(unittest.TestCase):
    def setUp(self):
        dummy_book = {
            "user_id": -1,
            "name": "Harry Potter and the Philosopher's Stone",
            "authors": ["J.K. Rowling"],
            "genres": ["Fantasy"],
            "published": 1997,
            "edition": 1,
            "condition": 1
        }
        body = {
            "op": "add_book",
            "book_model": dummy_book
        }
        result = asyncio.get_event_loop() \
                        .run_until_complete(client.send_test_message(body))
        self.__dummy_book = json.loads(result)
    
    """ Method that is run after every test case """
    def tearDown(self):
        body = { "op": "hard_delete_book", "book_id": int(self.__dummy_book["id"]) }
        asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        body = { "op": "decrement_next_book_id" }
        asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))

    """
        Tests if the 'add_book' method returns a valid new book object
    """
    def test_add_book_returns_book_model(self):
        self.assertEqual(type(self.__dummy_book), dict)
        self.assertTrue("id" in self.__dummy_book)
        self.assertTrue("user_id" in self.__dummy_book)
        self.assertTrue("name" in self.__dummy_book)
        self.assertTrue("authors" in self.__dummy_book)
        self.assertTrue("genres" in self.__dummy_book)
        self.assertTrue("published" in self.__dummy_book)
        self.assertTrue("edition" in self.__dummy_book)
        self.assertTrue("condition" in self.__dummy_book)
        
    
    """
        Tests if the 'add_books' method raises an error if the user_id given is
        invalid.
    """
    def test_add_book_exception_invalid_user_id(self):
        body = {
            "op": "add_book",
            "book_model": {
                "user_id": "3",
                "name": "Harry Potter and the Philosopher's Stone",
                "authors": ["J.K. Rowling"],
                "genres": ["Fantasy"],
                "published": 1997,
                "edition": 1,
                "condition": 1
            }
        }
        with self.assertRaises(Exception):
            asyncio.get_event_loop() \
                        .run_until_complete(client.send_test_message(body))
    
    """
        Tests if the 'add_books' method raises an error if the book model given
        is invalid.
    """
    def test_add_book_exception_invalid_book_model(self):
        body = {
            "op": "add_book",
            "book_model": {
                "user_id": 2,
                "name": "Harry Potter and the Philosopher's Stone",
                "authors": ["J.K. Rowling"],
                "genres": ["Fantasy"],
                "published": 1997,
                "edition": 1
            }
        }
        with self.assertRaises(Exception):
            asyncio.get_event_loop() \
                .run_until_complete(client.send_test_message(body))

    """ Tests if the remove_book method marks the book as removed. """
    def test_remove_book_marks_book_as_removed(self):
        # Try removing dummy book
        body = {
            "op": "remove_book",
            "book_id": self.__dummy_book["id"]
        }
        asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        
        # See if its status == 3
        body = {
            "op": "get_book_info",
            "book_id": self.__dummy_book["id"]
        }
        return_val = asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        book_model = json.loads(return_val)
        self.assertEqual(book_model["status"], 3)
    
    """ Tests if the get_book_info returns the correct book. """
    def test_get_book_info_returns_correct_book(self):
        # Get info from the book
        body = {
            "op": "get_book_info",
            "book_id": self.__dummy_book["id"]
        }
        return_val = asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        book_info = json.loads(return_val)

        # Check if info matches
        self.assertEqual(book_info["id"], self.__dummy_book["id"])
        self.assertEqual(book_info["user_id"], self.__dummy_book["user_id"])
        self.assertEqual(book_info["name"], self.__dummy_book["name"])
        self.assertListEqual(book_info["authors"], self.__dummy_book["authors"])
        self.assertListEqual(book_info["genres"], self.__dummy_book["genres"])
        self.assertEqual(book_info["published"], self.__dummy_book["published"])
        self.assertEqual(book_info["condition"], self.__dummy_book["condition"])
        self.assertEqual(book_info["status"], self.__dummy_book["status"])
    
    """ Tests if update_book method updates the book"""
    def test_update_book_returns_updated_book(self):
        # Log in a user
        body = {
            "op": "log_in_user",
            "user_id": -2
        }
        session_id = asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))

        # Update the book with an admin's session ID
        body = {
            "op": "update_book",
            "book_id": self.__dummy_book["id"],
            "session_id": session_id,
            "book_model": {
                "name": "Harry Potter and the Prisoner of Azkaban",
                "authors": ["J.K. Rowling"],
                "genres": ["Fantasy"],
                "published": 1999,
                "condition": 1,
                "edition": 3
            }
        }
        return_val = asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        updated_book = json.loads(return_val)

        # Check if updated_book is the updated book
        self.assertEqual(updated_book["name"], "Harry Potter and the Prisoner of Azkaban")
        self.assertEqual(updated_book["published"], 1999)
        self.assertEqual(updated_book["edition"], 3)
    
    """ Tests if filter_books method returns correct collection of books. """
    def test_filter_books_returns_filtered_books(self):
        body = {
            "op": "filter_books",
            "search_model": {
                "name": self.__dummy_book["name"],
                "authors": self.__dummy_book["authors"],
                "genres": self.__dummy_book["genres"],
                "published": self.__dummy_book["published"],
                "edition": self.__dummy_book["edition"],
                "condition": self.__dummy_book["condition"]
            }
        }
        return_val = asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        filtered_books = json.loads(return_val)
        for book in filtered_books.values():
            self.assertEqual(book["name"], self.__dummy_book["name"])
            self.assertListEqual(book["authors"], self.__dummy_book["authors"])
            self.assertListEqual(book["genres"], self.__dummy_book["genres"])
            self.assertEqual(book["published"], self.__dummy_book["published"])
            self.assertEqual(book["edition"], self.__dummy_book["edition"])
            self.assertEqual(book["condition"], self.__dummy_book["condition"])
    
    """ Tests if all_books method returns some books. """
    def test_get_all_books_returns_non_empty_dict(self):
        body = { "op": "get_all_books" }
        return_val = asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        all_books = json.loads(return_val)
        self.assertTrue(type(all_books) == dict)
        self.assertTrue(len(all_books) > 0)

    """ Tests if make_book_unavaliable makes book unavaliable. """
    def test_make_book_unavailable_makes_book_unavailable(self):
        body = {
            "op": "make_book_unavailable",
            "book_id": self.__dummy_book["id"]
        }
        asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        body = {
            "op": "get_book_info",
            "book_id": self.__dummy_book["id"]
        }
        result_val = asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        updated_book = json.loads(result_val)
        self.assertEqual(updated_book["status"], 2)
    
    """ Tests if make_book_avaliable makes book avaliable. """
    def test_make_book_available_makes_book_available(self):
        body = {
            "op": "make_book_unavailable",
            "book_id": self.__dummy_book["id"]
        }
        asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        body = {
            "op": "make_book_available",
            "book_id": self.__dummy_book["id"]
        }
        asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        body = {
            "op": "get_book_info",
            "book_id": self.__dummy_book["id"]
        }
        result_val = asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        updated_book = json.loads(result_val)
        self.assertEqual(updated_book["status"], 1)

if __name__ == "__main__":
    unittest.main()