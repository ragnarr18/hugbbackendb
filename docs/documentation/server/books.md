# Server - book functions

Here you can find all of the different book related functionality that the file [server.py](../../../src/app/server.py) makes available to the client. Within each JSON body, you must include a `"op"` value in order to specify the operation you want the server to perform. All properties are required unless a property is specified to be optional.

## Table of Contents

1. [add_book](#add_book)
2. [remove_book](#remove_book)
3. [get_book_info](#get_book_info)
4. [update_book](#update_book)
5. [filter_books](#filter_books)
6. [get_all_books](#get_all_books)
7. [make_book_unavailable](#make_book_unavailable)
8. [make_book_available](#make_book_available)
9. [hard_delete_book](#hard_delete_book)
10. [decrement_next_book_id](#decrement_next_book_id)

---

## add_book

### Description

Creates and stores a new book object within the system with the information sent in the request body. Once the book is created, it returns the newly created book object with its ID as well.

### Example JSON body

``` json
{
    "op": "add_book",
    "book_model": {
        "user_id": 3,
        "name": "Harry Potter and the Philosopher's stone",
        "authors": ["J.K. Rowling"],
        "genres": ["Fantasy"],
        "published": 1997,
        "edition": 1,
        "condition": 1
    }
}
```

### Property descriptions

| Name        | Type   | Description
|-------------|--------|------------
| book_model | Object | Holds the information of the new book.
| user_id | Number | The ID of the user who owns this book.
| name | String | The title of the book.
| author | List | Name of the author(s) of this book.
| genres | List | List of the names of the genres that this book belongs to.
| published | Number | The year the book was published.
| edition | Number | The edition of the book.
| condition | Number | Represents the condition of the book, **1: great**, **2: mediocre**, **3: bad** condition.

<br />

## remove_book

### Description

Marks the book with the given ID as "deleted" so other users, even its owner, cannot have access to it, ever. **Only the owner of the book or an admin can remove a book**. This step is irreversible and users who wish to attempt this should receive a fair warning before committing this heinous crime.

### Example JSON body

``` json
{
    "op": "remove_book",
    "book_id": 2
}
```

### Property descriptions

| Name        | Type   | Description
|-------------|--------|------------
| book_id | Number | The ID of the book to remove.

<br />

## get_book_info

### Description

Gets all the information related to a book with the given book ID. There should be no restrictions to this functionality, pretty much anyone should be able to get book information.

### Example JSON body

``` json
{
    "op": "get_book_info",
    "book_id": 2
}
```

### Property descriptions

| Name        | Type   | Description
|-------------|--------|------------
| book_id | Number | The ID of the book to get info about.

<br />

## update_book

### Description

Updates the information of the book with the given ID. **This functionality should only be available to a book's owner**. Every property needs to be present in the request body and every property is updated (no partial update, only full update). However, you can insert either an empty string, list, 0 or `null` for properties you don't want to change.

### Example JSON body

``` json
{
    "op": "update_book",
    "book_id": 2,
    "session_id": "_25dsjk2j35j32læj2klj1lj53jl1_",
    "book_model": {
        "name": "Harry Potter and the Chamber of Secrets",
        "authors": ["J.K. Rowling"],
        "genres": ["Fantasy"],
        "published": 1999,
        "edition": 1,
        "condition": 2
    }
}
```

### Property descriptions

| Name        | Type   | Description
|-------------|--------|------------
| book_id | Number | The ID of the book to update.
| session_id | String | The session ID for the user (if he's logged in) so the server can authenticate him.
| book_model | Object | Holds the updated information for a certain book.
| name | String | The updated title of the book.
| authors | List | A list of the updated authors for the book.
| genres | List | A list of the updated genres for the book.
| published | Number | The updated year the book was published.
| edition | Number | The updated edition of the book.
| condition | Number | The updated condition of the book. **1: great**, **2: mediocre** and **3: bad** condition.

<br />

## filter_books

### Description

Returns a list of *basic* information of all books that fulfil a certain search criteria/filter. If you don't want to search by a certain property, then you can include it, but leave it empty if it's a string, or a 0 if it's an int.

### Example JSON body

``` json
{
    "op": "filter_books",
    "search_model": {
        "name": "Harry Potter and the Philosopher's Stone",
        "authors": ["J.K. Rowling"],
        "genres": ["Fantasy"],
        "published": 1999,
        "edition": 1,
        "condition": 1
    }
}
```

### Property descriptions

| Name        | Type   | Description
|-------------|--------|------------
| search_model | Object | Holds the search criteria to filter the books with.
| name | String | The title of the book to search for.
| authors | List | A list of the names of the authors to search for.
| genres | List | List of the names of the genres to search for.
| published | Number | The year the book was published.
| edition | Number | The edition of the book.
| condition | Number | The condition the book should be in, **1: great**, **2: mediocre** and **3: bad** condition.

<br />

## get_all_books

### Description

Reads and returns all the information the system has on every *available* book there is to find. The books are set up as an array of `Book` objects.

### Example JSON body

``` json
{
    "op": "get_all_books"
}
```

### Property descriptions

There are no extra properties to define.

<br />

## make_book_unavailable

### Description

Marks a book with the given book ID as available, therefore it will not show up in searches.

### Example JSON body

``` json
{
    "op": "make_book_unavailable",
    "book_id": 3
}
```

### Property descriptions

| Name        | Type   | Description
|-------------|--------|------------
| book_id | Number | The ID of the book to mark as unavailable.

<br />

## make_book_available

### Description

Marks a book with the given ID as available and will, therefore appear in searches results.

### Example JSON body

``` json
{
    "op": "make_book_available",
    "book_id": 3
}
```

### Property descriptions

| Name        | Type   | Description
|-------------|--------|------------
| book_id | Number | The ID of the book to mark as available.

<br />

## hard_delete_book 

> **WARNING**: This method should only be used in test files, not in production.

### Description

Instead of just marking a book as deleted, this method *actually* deletes a book from the system, so be careful with it.

### Example JSON body

``` json
{
    "op": "hard_delete_book",
    "book_id": 3
}
```

### Property descriptions

| Name        | Type   | Description
|-------------|--------|------------
| book_id | Number | The ID of the book to delete.

<br />

## decrement_next_book_id

> **WARNING**: This method should only be used in test files, not in production.

### Description

Decrements the next available ID for books, should only be used in test files.

### Example JSON body

``` json
{
    "op": "decrement_next_book_id"
}
```

### Property descriptions

There are no extra properties to describe.